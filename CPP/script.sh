#!/bin/bash
g++ /develop/main.cpp -Wreturn-type -o /develop/main 1> /develop/compilation/compilation-output.txt 2>/develop/compilation/compilation-error.txt
compilation_output=$?
echo $compilation_output > /develop/compilation/compilation-exitcode.txt

if [ $compilation_output == 0 ] 
then
    /develop/main < /develop/execution/input.txt 1> /develop/execution/execution-output.txt 2>/develop/execution/execution-error.txt
    echo $? > /develop/execution/execution-exitcode.txt
fi

exit $?